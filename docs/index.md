# Python 사전:: 생성, 맥세스 및 메소드 <sup>[1](#footnote_1)</sup>

> <font size="3">키와 값의 매핑인 사전을 만들고 액세스하는 방법에 대해 알아본다.</font>

## 목차

1. [들어가며](./dictionaries.md#intro)
1. [Python에서 사전이란?](./dictionaries.md#sec_02)
1. [Python에서 사전 생성 방법](./dictionaries.md#sec_03)
1. [사전에서 값을 액세스하는 방법](./dictionaries.md#sec_04)
1. [Python에서 사전을 수정하는 방법](./dictionaries.md#sec_05)
1. [Python에서 사전을 반복하는 방법](./dictionaries.md#sec_06)
1. [Python에서 사전 메서드 사용법](./dictionaries.md#sec_07)
1. [마치며](./dictionaries.md#conclusion)


<a name="footnote_1">1</a>: [Python Tutorial 12 — Python Dictionaries: Creation, Access, and Methods](https://levelup.gitconnected.com/python-tutorial-12-python-dictionaries-creation-access-and-methods-c84211411170?sk=054150a864528e95dc7b13254e6a1b8c)를 편역한 것이다.
