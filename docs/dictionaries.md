# Python 사전:: 생성, 맥세스 및 메소드

## <a name="intro"></a> 들어가며
이 포스팅에서는 키를 값에 매핑하는 사전을 만들고 액세스하는 방법을 설명할 것이다. 또한 사전을 수정하는 방법과 Python이 사전 작업에 제공하는 내장된 메서드를 사용하는 방법도 다룰 것이다.

Python에서 사전은 가장 유용하고 다재다능한 데이터 구조 중 하나이다. 그것들은 효율적이고 유연한 방식으로 데이터를 저장하고 검색할 수 있게 해준다. 사전을 사용하여 사용자, 제품 또는 설정과 같은 다양한 객체에 대한 정보를 저장할 수 있다. 또한 캐싱, 메모화 또는 해싱과 같은 알고리즘을 구현하기 위해 사전을 사용할 수 있다.

이 포스팅이 끝날 때에는 다음 작업을 수행할 수 있을 것이다.

- 다양한 방법을 사용하여 사전 만들기
- 키 또는 메서드를 사용하여 사전의 값 액세스
- 키-값 쌍을 추가, 업데이트 또는 삭제하여 사전 수정
- 루프 또는 컴프리헨션을 사용하여 사전을 반복
- 사전 메서드를 사용하여 사전에서 일반적인 작업 수행

시작하기 전에 컴퓨터에 Python이 설치되어 있는지 확인한다. [공식 웹사이트](https://www.python.org/)에서 Python을 다운로드할 수 있다. 또한 [Repl.it](https://replit.com/) 또는 [Trinket](https://trinket.io/) 같은 온라인 Python 인터프리터를 사용하여 이 튜토리얼의 코드 예를 실행할 수 있다.

Python 사전을 시작할 준비가 되었나요? 시작해 보자!

## <a name="sec_02"></a> Python에서 사전이란?
사전은 Python에서 키-값 쌍의 집합을 저장하는 데이터 구조이다. 키-값 쌍은 이름과 전화번호, 또는 상품과 가격과 같이 관련된 두 정보의 쌍이다. 키는 해당 값에 접근하기 위해 사용되는 고유한 식별자이다. 값은 문자열, 숫자, 목록 또는 다른 사전과 같이 사전에 저장될 수 있는 모든 데이터이다.

사전은 다른 프로그래밍 언어에서 **매핑** 또는 **연관 배열(associative array)**이라고도 한다. 이들은 Python에서 값의 집합을 저장하는 또 다른 데이터 구조인 리스트와 유사하다. 그러나 사전은 리스트와 달리 순서가 없으므로 키-값 쌍의 순서는 중요하지 않다. 사전은 또한 **변경이 가능**하며, 이는 사전이 생성된 후에 변경될 수 있다는 것을 의미한다.

사전은 데이터를 효율적이고 유연한 방식으로 저장하고 검색하는 데 매우 유용하다. 사전을 사용하여 사용자, 제품 또는 설정과 같은 다양한 객체에 대한 정보를 저장할 수 있다. 사전을 사용하여 캐싱, 메모화 또는 해싱과 같은 알고리즘을 구현할 수도 있다.

다음은 사용자에 대한 일부 정보를 저장하는 Python 사전의 예이다.

```python
# Create a dictionary
user = {
    "name": "Alice",
    "age": 25,
    "email": "alice@example.com",
    "phone": "123-456-7890"
}

# Print the dictionary
print(user)
```

이 코드의 결과는 다음과 같다.

```
{'name': 'Alice', 'age': 25, 'email': 'alice@example.com', 'phone': '123-456-7890'}
```

이 예에서 사전은 중괄호 `{ }`를 사용하여 만들 수 있다. 각 키-값 쌍은 쉼표로 구분되며, 각 쌍은 키와 콜론으로 구분된 값으로 구성된다. 키는 `name`, `age`, `email`, `phone`이며, 값은 `Alice`, `25`, `alice@example.com`, `123–456–7890`이다. 키는 문자열, 숫자 또는 튜플과 같은 불변의 데이터 타입일 수 있다. 값은 문자열, 숫자, 목록 또는 다른 사전과 같은 모든 데이터 타입일 수 있다.

[다음 절](#sec_02)에서는 Python에서 다양한 방법을 사용하여 사전을 만드는 방법을 설명할 것이다.

## <a name="sec_03"></a> Python에서 사전 생성 방법
[앞 절](#intro)에서 Python에서 중괄호 `{}`룰 사용하여 사전을 만드는 방법의 예를 보았다. 이것은 사전을 만드는 가장 일반적인 방법 중 하나이지만 이것만은 아니다. 이 절에서는 다른 방법을 사용하여 Python에서 사전을 만드는 방법을 보일 것이다.

Python에서 사전을 만드는 데 사용할 수 있는 몇 가지 방법은 다음과 같다.

- `dict()` 생성자 사용
- `zip()` 기능 사용
- dict 컴프리헨션 구문 사용
- `fromkeys()` 메서드 사용

이들 각각의 메서드를 구체적으로 살펴보고 그 사용법의 예를 살펴보자.

```python
# Using the dict() constructor
# This method takes a sequence of key-value pairs as arguments
# and returns a dictionary object
my_dict = dict(name="Alice", age=25, country="USA")
print(my_dict) # {'name': 'Alice', 'age': 25, 'country': 'USA'}

# Using the zip() function
# This method takes two sequences of equal length as arguments
# and returns a dictionary object that maps the elements of the first sequence
# to the elements of the second sequence
keys = ["red", "green", "blue"]
values = [1, 2, 3]
my_dict = dict(zip(keys, values))
print(my_dict) # {'red': 1, 'green': 2, 'blue': 3}

# Using the dict comprehension syntax
# This method uses a for loop and an optional if condition to generate
# key-value pairs and create a dictionary object
my_dict = {x: x**2 for x in range(1, 6) if x % 2 == 0}
print(my_dict) # {2: 4, 4: 16}

# Using the fromkeys() method
# This method takes a sequence of keys and an optional value as arguments
# and returns a dictionary object that assigns the same value to all the keys
my_dict = dict.fromkeys(["a", "b", "c"], 0)
print(my_dict) # {'a': 0, 'b': 0, 'c': 0}
```

## <a name="sec_04"></a> 사전에서 값을 액세스하는 방법
일단 사전을 만든 다음에는 그 사전에 저장된 값을 액세스할 수도 있다. 사전에서 값을 액세스할 수 있는 방법은 여러 가지가 있는다. 이는 무엇을 할 것인가에 따라 다르다. 이 절에서는 다음과 같은 방법을 사용하여 사전에서 값을 액세스하는 방법을 설명할 것이다.

- **key키**를 인덱스로 사용
- `get()` 메서드 사용
- `value()` 메서드 사용
- `items()` 메소드 사용

이들 각각의 메서드를 구체적으로 살펴보고 그 사용법의 예를 살펴보자.

```python
# Using the key as an index
# This method allows you to access the value of a dictionary item by placing the key inside square brackets
country_capitals = {
    "United States": "Washington D.C.",
    "Italy": "Rome",
    "England": "London"
}
print(country_capitals["United States"]) # Washington D.C.
print(country_capitals["England"]) # London

# Using the get() method
# This method returns the value of a dictionary item given its key as an argument
# If the key does not exist, it returns a default value, which is None by default
print(country_capitals.get("France")) # None
print(country_capitals.get("France", "Not found")) # Not found

# Using the values() method
# This method returns a view of all the values in the dictionary
# You can iterate over them using a for loop or convert them to a list
for value in country_capitals.values():
    print(value) # Washington D.C., Rome, London
values_list = list(country_capitals.values())
print(values_list) # ['Washington D.C.', 'Rome', 'London']

# Using the items() method
# This method returns a view of all the key-value pairs in the dictionary as tuples
# You can iterate over them using a for loop or convert them to a list
for key, value in country_capitals.items():
    print(key, value) # United States Washington D.C., Italy Rome, England London
items_list = list(country_capitals.items())
print(items_list) # [('United States', 'Washington D.C.'), ('Italy', 'Rome'), ('England', 'London')]
```

## <a name="sec_05"></a> Python에서 사전을 수정하는 방법
[앞 절](#sec_04)에서는 여러 가지 방법을 사용하여 사전의 값에 접근하는 방법을 배웠다. 이 절에서는 키-값 쌍을 추가하거나 업데이트하거나 삭제하는 등 Python에서 사전을 수정하는 방법을 다룰 것이다. 또한 Python에서 사전을 수정할 때 제공하는 내장 메서드를 사용하는 방법도 보일 것이다.

사전은 생성된 후에 변경할 수 있는 것을 의미하는 변경 가능한 데이터 구조이다. Python에서 사전을 수정할 수 있는 방법은 다음과 같다.

- **할당 연산자(`=`)** 사용 
- `update()` 메서드 사용
- `pop()` 메서드 사용
- `popitem()` 메서드 사용
- `clear()` 메서드 사용
- `del` 키워드 사용

이들 각각의 메서드를 구체적으로 살펴보고 그 사용법의 예를 살펴보자.

```python
# Using the assignment operator (=)
# This method allows you to add a new item or update an existing item in the dictionary
# by assigning a value to a key
country_capitals = {
    "United States": "Washington D.C.",
    "Italy": "Naples",
    "England": "London"
}
# add a new item with "Germany" as key and "Berlin" as value
country_capitals["Germany"] = "Berlin"
# update the value of "Italy" key to "Rome"
country_capitals["Italy"] = "Rome"
print(country_capitals) # {'United States': 'Washington D.C.', 'Italy': 'Rome', 'England': 'London', 'Germany': 'Berlin'}

# Using the update() method
# This method takes another dictionary as an argument and adds or updates its key-value pairs
# to the original dictionary
country_capitals = {
    "United States": "Washington D.C.",
    "Italy": "Rome",
    "England": "London"
}
# add or update multiple items at once
country_capitals.update({
    "France": "Paris",
    "Spain": "Madrid",
    "Italy": "Milan"
})
print(country_capitals) # {'United States': 'Washington D.C.', 'Italy': 'Milan', 'England': 'London', 'France': 'Paris', 'Spain': 'Madrid'}

# Using the pop() method
# This method removes and returns the value of a specified key from the dictionary
# If the key does not exist, it returns a default value (None by default)
country_capitals = {
    "United States": "Washington D.C.",
    "Italy": "Milan",
    "England": "London"
}
# remove and get the value of "Italy" key
italy = country_capitals.pop("Italy")
print(italy) # Milan
print(country_capitals) # {'United States': 'Washington D.C.', 'England': 'London'}
# try to remove and get the value of "Germany" key, which does not exist
germany = country_capitals.pop("Germany", "Not found")
print(germany) # Not found

# Using the popitem() method
# This method removes and returns the last inserted key-value pair from the dictionary
# If the dictionary is empty, it raises a KeyError
country_capitals = {
    "United States": "Washington D.C.",
    "Italy": "Milan",
    "England": "London"
}
# remove and get the last inserted item
last_item = country_capitals.popitem()
print(last_item) # ('England', 'London')
print(country_capitals) # {'United States': 'Washington D.C.', 'Italy': 'Milan'}

# Using the clear() method
# This method removes all the items from the dictionary, leaving it empty
country_capitals = {
    "United States": "Washington D.C.",
    "Italy": "Milan",
    "England": "London"
}
# clear the dictionary
country_capitals.clear()
print(country_capitals) # {}

# Using the del keyword
# This keyword deletes an item or the entire dictionary from memory
country_capitals = {
    "United States": "Washington D.C.",
    "Italy": "Milan",
    "England": "London"
}
# delete the item with "Italy" key
del country_capitals["Italy"]
print(country_capitals) # {'United States': 'Washington D.C.', 'England': 'London'}
# delete the entire dictionary
del country_capitals
# print(country_capitals) # NameError: name 'country_capitals' is not defined
```

## <a name="sec_06"></a> Python에서 사전을 반복하는 방법
[이전 절](#sec_05)에서, Python에서 여러 가지 방법을 사용하여 사전을 수정하는 방법을 배웠다. 이 절에서, 사전의 키와 값을 순환시키는 것을 의미하는 Python에서 사전에서 반복하는 방법을 설명할 것이다. 또한 Python이 사전에서 반복하는 데 제공하는 내장 메서드를 사용하는 방법도 보일 것이다.

사전에서 반복하는 것은 사전의 각 키-값 쌍에 대해 인쇄, 계산, 필터링 또는 변환과 같은 작업을 수행할 때 유용하다. Python에서 다음과 같은 방법을 사용하여 사전에서 반복할 수 있다.

- `for` 루프 사용
- `key()` 메서드 사용
- `value()` 메서드 사용
- `items()` 메소드 사용
- **사전 컴프리헨션** 사용하기

이들 각각의 메서드를 구체적으로 살펴보고 그 사용법의 예를 보인다.

```python
# Using a for loop
# This method allows you to loop through the keys of a dictionary by default
# You can access the values by using the keys as indexes
my_dict = {"name": "Alice", "age": 25, "country": "USA"}
for key in my_dict:
    value = my_dict[key]
    print(key, value) # name Alice, age 25, country USA

# Using the keys() method
# This method returns a view object that contains the keys of the dictionary
# You can iterate over it or convert it to a list
my_dict = {"name": "Alice", "age": 25, "country": "USA"}
keys = my_dict.keys()
print(keys) # dict_keys(['name', 'age', 'country'])
for key in keys:
    print(key) # name, age, country
keys_list = list(keys)
print(keys_list) # ['name', 'age', 'country']

# Using the values() method
# This method returns a view object that contains the values of the dictionary
# You can iterate over it or convert it to a list
my_dict = {"name": "Alice", "age": 25, "country": "USA"}
values = my_dict.values()
print(values) # dict_values(['Alice', 25, 'USA'])
for value in values:
    print(value) # Alice, 25, USA
values_list = list(values)
print(values_list) # ['Alice', 25, 'USA']

# Using the items() method
# This method returns a view object that contains the key-value pairs of the dictionary as tuples
# You can iterate over it or convert it to a list
my_dict = {"name": "Alice", "age": 25, "country": "USA"}
items = my_dict.items()
print(items) # dict_items([('name', 'Alice'), ('age', 25), ('country', 'USA')])
for key, value in items:
    print(key, value) # name Alice, age 25, country USA
items_list = list(items)
print(items_list) # [('name', 'Alice'), ('age', 25), ('country', 'USA')]

# Using a dictionary comprehension
# This method allows you to create a new dictionary from an existing one by applying some logic or transformation to the keys and values
# The syntax is {key: value for key, value in old_dict.items() if condition}
my_dict = {"name": "Alice", "age": 25, "country": "USA"}
# create a new dictionary with only the keys that start with 'a'
new_dict = {key: value for key, value in my_dict.items() if key.startswith('a')}
print(new_dict) # {'age': 25}
# create a new dictionary with the values in uppercase
new_dict = {key: value.upper() for key, value in my_dict.items()}
print(new_dict) # {'name': 'ALICE', 'age': '25', 'country': 'USA'}
```

## <a name="sec_07"></a> Python에서 사전 메서드 사용법
[이전 절](#sec_06)에서는 Python에서 여러 가지 메서드를 사용하여 사전을 만들고 접근하고 수정하는 방법을 배웠다. 이 절에서는 Python에서 사전 작업을 위해 제공하는 내장 메서드를 사용하는 방법을 보일 것이다. 이 방법들은 사전의 복사, 정렬, 병합 또는 사전의 길이 같은 일반적인 작업을 사전에서 수행하는 데 유용하다.

Python에서 사전 작업에 사용할 수 있는 메서드들은 다음과 같다.

- `copy()` 메서드를 사용하여 사전의 얕은 복사본 만들기
- `sorted()` 함수를 사용하여 키 또는 값으로 사전 정렬
- `len()` 함수를 사용하여 사전에서 키-값 쌍의 갯수를 얻는다
- `in` 연산자를 사용하여 사전에 키 또는 값이 있는지 확인
- `max()`와 `min()` 함수를 사용하여 사전에서 최대와 최소 키 또는 값 찾기
- `sum()` 함수를 사용하여 사전에서 값의 합을 구한다

이들 각각의 메서드를 구체적으로 살펴보고 그 사용법의 예를 살펴보자.

```python
# Using the copy() method to create a shallow copy of a dictionary
# This method returns a new dictionary that contains the same key-value pairs as the original dictionary
# However, the new dictionary is not linked to the original dictionary, so any changes made to one will not affect the other
my_dict = {"name": "Alice", "age": 25, "country": "USA"}
# create a copy of my_dict
my_copy = my_dict.copy()
print(my_copy) # {'name': 'Alice', 'age': 25, 'country': 'USA'}
# change the value of "age" key in my_copy
my_copy["age"] = 30
print(my_copy) # {'name': 'Alice', 'age': 30, 'country': 'USA'}
print(my_dict) # {'name': 'Alice', 'age': 25, 'country': 'USA'}

# Using the sorted() function to sort a dictionary by keys or values
# This function returns a sorted list of the keys or values of the dictionary
# The function takes a key argument that specifies a function to be called on each element before sorting
# By default, the function sorts the elements in ascending order, but this can be reversed by setting the reverse argument to True
my_dict = {"name": "Alice", "age": 25, "country": "USA"}
# sort the dictionary by keys
sorted_keys = sorted(my_dict)
print(sorted_keys) # ['age', 'country', 'name']
# sort the dictionary by values
sorted_values = sorted(my_dict, key=lambda x: my_dict[x])
print(sorted_values) # ['age', 'name', 'country']
# sort the dictionary by values in descending order
sorted_values = sorted(my_dict, key=lambda x: my_dict[x], reverse=True)
print(sorted_values) # ['country', 'name', 'age']

# Using the len() function to find the number of key-value pairs in a dictionary
# This function returns the number of items in the dictionary
my_dict = {"name": "Alice", "age": 25, "country": "USA"}
# get the length of the dictionary
print(len(my_dict)) # 3

# Using the in operator to check if a key or a value exists in a dictionary
# This operator returns True if the key or the value is present in the dictionary, and False otherwise
my_dict = {"name": "Alice", "age": 25, "country": "USA"}
# check if "name" is a key in the dictionary
print("name" in my_dict) # True
# check if "Alice" is a value in the dictionary
print("Alice" in my_dict.values()) # True
# check if "Bob" is a value in the dictionary
print("Bob" in my_dict.values()) # False

# Using the max() and min() functions to find the maximum and minimum keys or values in a dictionary
# These functions return the largest or smallest element in the dictionary, based on the keys or values
# The functions take a key argument that specifies a function to be called on each element before comparison
my_dict = {"name": "Alice", "age": 25, "country": "USA"}
# find the maximum key in the dictionary
print(max(my_dict)) # name
# find the minimum key in the dictionary
print(min(my_dict)) # age
# find the maximum value in the dictionary
print(max(my_dict, key=lambda x: my_dict[x])) # country
# find the minimum value in the dictionary
print(min(my_dict, key=lambda x: my_dict[x])) # age

# Using the sum() function to find the sum of the values in a dictionary
# This function returns the sum of the values in the dictionary, if they are numeric
# The function takes a start argument that specifies the initial value to be added to the sum
my_dict = {"a": 1, "b": 2, "c": 3}
# find the sum of the values in the dictionary
print(sum(my_dict.values())) # 6
# find the sum of the values in the dictionary, starting from 10
print(sum(my_dict.values(), 10)) # 16
```

## <a name="conclusion"></a> 마치며
Python 사전에 대한 이 포스팅의 끝에 도착했다. 이 포스팅에서는 키를 값에 매핑한 사전을 만들고 액세스하는 방법을 배웠다. 또한 사전을 수정하는 방법과 Python이 사전 작업에 제공하는 내장 메서드를 사용하는 방법도 설명하였다.

이제 다음과 같은 일을 할 수 있을 것이다.

- `{}`, `dict()`, `zip()`, dict 커프리헨션 또는 `key()` 같은 다양한 메서드을 사용하여 사전을 만든다
- `keys`, `get()`, `values()` 또는 `items()`을 사용하여 사전에서 값 액세스
- 할당 연산자, `update()`, `pop()`, `popitem()`, `clear()` 또는 `del`을 사용하여 키-값 쌍을 추가, 업데이트 또는 삭제하여 사전 수정
- 루프, `keys()`, `values()`, `items()` 또는 사전 커프리헨션을 사용하여 사전에 반복 작업 수행
- `copy()`, `sorted()`, `len()`, `in`, `max()`, `min()` 또는 `sum()` 같은 사전 메서드를 사용하여 사전에서 일반적인 작업을 수행
